from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

from lab_3.forms import FriendForm
from lab_1.models import Friend

# Create your views here.
@login_required(login_url = "/admin/login/")
def index(request):
    friendss = Friend.objects.all()
    response = {'friends': friendss}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = "/admin/login/")
def add_friend(request):
    context ={}
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)

    if request.method == "POST":
        # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect('/lab-3/')


    context['form']= form
    return render(request, "lab3_form.html", context)