from django.urls import path
from django.urls.resolvers import URLPattern

from lab_1.views import friend_list
from .views import index, add_friend

urlpatterns = [
    path('', index, name = 'index'),
    path('add', add_friend),
]