from django import forms
  
from lab_1.models import Friend
  
# create a ModelForm
class FriendForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Friend
        fields = "__all__"