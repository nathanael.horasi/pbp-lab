from lab_2.views import index
from django.urls import path
from django.urls.resolvers import URLPattern
from .views import index, xml, json

urlpatterns = [
    path('', index),
    path('xml', xml),
    path('json', json)
]
